# Scaling
Scaling is key consideration when we build an enterprise level project. An enterprise level program should be able to handle large number of requests at once and respond quickly.
&nbsp;
## Types of Scaling
### 1. Vertical Scaling
- Vertical scaling increases the scale of a system by adding more power to existing machine.
- In our project this would mean to adding more RAM or CPU. 
- As we can see, there is a limitation since you can’t keep adding resources to one server beyond a point.

### 2. Horizontal Scaling
- Horizontal scaling increases the scale of a system by adding more machines.
- In our project this can be done by collecting and connecting multiple machines to take on more requests.
- We need the right computing infrastructure for implementing this.

&nbsp;
## Comparison


| Horizontal Scaling| Vertical Scaling|
| ----------- | ----------- |
| Load balancing required | Load balancing unnecessary |
| Resilient to system failure | Single point of failure |
| Data Inconsistency  | Data Consistent |
| Scales well | Hardware limit |

&nbsp;
## Factors to consider for Scaling type
- ### Load Balancing
  Horizontal scaling requires load balancing, meaning the system must also incorporate a component that spreads traffic among the machines equally. This component is obviously unnecessary in the vertical, single machine solution.

- ### Failure Resilience
  Horizontal scaling is more resistant to system failure because the load is spread across multiple machines. There is only one point of failure in a vertically scaled system.

- ### Data Consistency
  Horizontal scaling means different machines are handling different requests which may lead to their data become inconsistence. Meanwhile, a vertically scaled machine doesn’t suffer this issue because of single machine.

- ### Limit
  There is not really a limit to horizontal scaling, beyond budget and space. we can add as many machine as we want. However, a single machine can only be improved upon until it reaches the current limits of computing.

&nbsp;
## Conclusion

- With a better understanding of the options, what should we use for our project? The answer is both. With comapny's capitalization we can vertically scale machine and use horizontally scaling on top of them. It is the hybrid approach of combining speed and data consistency of vertical sacling, with failure resilience and infinite scalablility of horizontal scaling.


&nbsp;
## Reference
- www.geeksforgeeks.org
- www.devteam.space
